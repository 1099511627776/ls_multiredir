<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

class PluginMultiredir_HookUrl extends Hook {

    public function RegisterHook() {
        $this->AddHook('init_action', 'InitAction');
    }

    private function Redirect($old_id){
		if($new_id = $this->PluginMultiredir_Moduleredir_getRedir($old_id)){
	    	//dump('multiredir:::');
			//dump($old_id);
			//dump($new_id);
			$oTopic = $this->Topic_GetTopicById($new_id);
			//dump($oTopic);
			Router::Location($oTopic->getUrl());
		};
    }
    public function InitAction() {
    	if(Router::GetAction() == 'blog'){
			//dump(Router::GetParams());
			if(preg_match('/(\d+)\.html/i',Router::GetActionEvent(),$matches)){
				//dump('redirect personal');
				$old_id = $matches[1];
				$this->Redirect($old_id);
			} elseif(preg_match('/(\d+)\.html/i',Router::GetParam(0),$matches)) {
				//dump('redirect open');
				$old_id = $matches[1];
				$this->Redirect($old_id);
			}
    	}
    }
}
?>