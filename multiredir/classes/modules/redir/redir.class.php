<?php

/* -------------------------------------------------------
 *
 *   LiveStreet (v1.0)
 *   Copyright � 2012 1099511627776@mail.ru
 *
 * --------------------------------------------------------
 *
 *   Contact e-mail: 1099511627776@mail.ru
 *
  ---------------------------------------------------------
*/

class PluginMultiredir_Moduleredir extends Module
{
    protected $oMapper;

	public function Init(){
		$this->oMapper = Engine::GetMapper(__CLASS__, 'redir');
	}                       	

	public function getRedir($iid){
		return $this->oMapper->getRedir($iid);
	}

}

?>