<?php

class PluginMultiredir_ModuleRedir_MapperRedir extends Mapper
{

    public function getRedir($old_id)
    {
		$sql = "SELECT new FROM ".Config::Get('plugin.multiredir.table')." WHERE old = ?d";
		if ($aRow = $this->oDb->selectRow($sql,$old_id)) {
		    return $aRow['new'];
		}
		return false;
	}


}
?>