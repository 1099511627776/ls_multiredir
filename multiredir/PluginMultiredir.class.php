<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
	die('Hacking attempt!');
}

class PluginMultiredir extends Plugin {
	
	/**
	 * Активация плагина	 
	 */
	public function Activate() {		
        if (!$this->isTableExists('prefix_dup')) {
            $this->ExportSQL(dirname(__FILE__).'/install.sql');
        }        
		return true;
	}
	public function Deactivate() {		
		return true;
	}
	
	public function Init() {
	}
}
?>