--
CREATE TABLE IF NOT EXISTS `prefix_dup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `old` int(11) unsigned DEFAULT NULL,
  `new` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
